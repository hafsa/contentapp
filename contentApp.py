from webApp import WebApp

class ContentApp(WebApp):
    def __init__(self, host, port):
        super().__init__(host, port)
        self.content_dict = {
            '/': '<html><body><h1>Main page: write hola or adios please</h1></body></html>',
            '/hola': '<html><body><h1>Hello World! What do you need?</h1></body></html>',
            '/adios': '<html><body><h1>Bye World! See you soon!</h1></body></html>'}

    def process(self, analyzed):
        resource = analyzed['received']
        if resource in self.content_dict:
            http = "200 OK"
            html = self.content_dict[resource]
        else:
            http = "404 Not Found"
            html = "<html><body><h1>404 Not Found</h1></body></html>"
        return http, html

if __name__ == '__main__':
    web_app = ContentApp('', 2020)
    web_app.accept_clients()
